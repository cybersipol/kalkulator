﻿using System;
using System.Net.NetworkInformation;
using System.Xml.Schema;
using Wojtek01.Animals;

namespace Wojtek01
{
    class Program
    {
        public enum Dzialanie
        {
            Dodawanie = 1,
            Odejmowanie = 2,
            Mnozenie = 3,
            Dzielenie = 4,
            ResztaZDzielenia = 5,
            Potegowanie = 6,
            Maksymalna = 7,
            Minimalna = 8,
            WyczyscWynik = 9,
            Wyjscie = 0,
            Inny = -1
        }

        static Dzialanie[] tablicaDzialan = { Dzialanie.Dodawanie, 
                                                Dzialanie.Odejmowanie, 
                                                Dzialanie.Mnozenie, 
                                                Dzialanie.Dzielenie, 
                                                Dzialanie.ResztaZDzielenia, 
                                                Dzialanie.Potegowanie,
                                                Dzialanie.Maksymalna,
                                                Dzialanie.Minimalna,
                                                Dzialanie.WyczyscWynik, 
                                                Dzialanie.Wyjscie };
        
        static double? wynik = null;
        
        public static string OpisDzialania( Dzialanie dzialanie )
        {
            switch ( dzialanie )
            {
                case Dzialanie.Dodawanie: return "Dodawanie";
                case Dzialanie.Odejmowanie: return "Odejmowanie";
                case Dzialanie.Mnozenie: return "Mnożenie";
                case Dzialanie.Dzielenie: return "Dzielenie";
                case Dzialanie.ResztaZDzielenia: return "Reszta z dzielenia";
                case Dzialanie.Potegowanie: return "Potęgowanie";
                case Dzialanie.Maksymalna: return "Max. liczba";
                case Dzialanie.Minimalna: return "Min. liczba";
                case Dzialanie.WyczyscWynik: return "Wyczyść wynik";
                case Dzialanie.Wyjscie: return "Wyjście";
                default:
                    return "jakaś inna opcja";
            }
        }

        
        public static void Main( string[] args )
        {
            var sredniwiekLabladora = Lablador.pobierzSredniWiek(); 

            var animals = new Animal[]
            {
                new Lablador( "Marko", "Biszkoptowy", 6 ),
                new Amstaf( "Pysia",  9 ),
                new Horse( "Baska", "Gniady", 10 ),
                new Fish( "Nemo", true, "Ornage", 1 ),
            };

            var ami = new Lablador( "Ami", "Biszkoptowy", 6 );
            Console.WriteLine("Wiek {1} ma: {0} lat", ami.Years, ami.Name);

            var stado = new Stado( new ILandAnimal[] 
                    { 
                        ami,
                        new Horse( "Baska", "Gniady", 10 ),
                        new Amstaf( "Pysia",  9 )
                        
                 } );        
            
            //var _animal = new Animal( "some" ); 

                
            
            foreach ( var animal in animals )
            {
//                string name = animal.Name;
                Console.WriteLine(animal.ToString());
            }
            
            Console.WriteLine("----- STADO -----");
            Console.WriteLine( stado );
                
            
            /*
            // Ami
            Console.WriteLine();
            
            Animal ami = new Animal("Ami");
            
            ami.Name = "Ami";
            ami.Color = "Biszkoptowy";
            ami.Years = 6;

            Console.WriteLine("--- {0} : {1} ---", ami.Name, ami.GetType().Name);
            Console.WriteLine("Wiek {0}, kolor: {1}",ami.Years, ami.Color);
            Console.WriteLine("Dzwięk: {0}",ami.Sound());
            Console.WriteLine("Poruszanie: {0}",ami.Move());

            // Pysia
            Console.WriteLine();
            Animal pysia = new Animal("Pysia");
            pysia.Name = "Pysia";
            //pysia.Color = "brazowy";
            pysia.Years = 9;


          
            Console.WriteLine("--- {0} : {1} ---", pysia.Name, pysia.GetType().Name);
            Console.WriteLine("Wiek {0}, kolor: {1}",pysia.Years, pysia.Color);
            Console.WriteLine("Dzwięk: {0}",pysia.Sound());
            Console.WriteLine("Poruszanie: {0}",pysia.Move());

            //Kon
            Console.WriteLine();
            Animal kon = new Animal("Baśka") { Name = "baska", Color = "gniady", Years = 10 };
            Console.WriteLine("--- {0} : {1} ---", kon.Name, kon.GetType().Name);
            Console.WriteLine("Wiek {0}, kolor: {1}",kon.Years, kon.Color);
            Console.WriteLine("Dzwięk: {0}",kon.Sound());
            Console.WriteLine("Poruszanie: {0}",kon.Move());

            //Nemo
            Console.WriteLine();
            Animal nemo = new Animal("Nemo") { Color = "pomarnczowy", Years = 1 };
            Console.WriteLine("--- {0} : {1} ---", nemo.Name, nemo.GetType().Name);
            Console.WriteLine("Wiek {0}, kolor: {1}",nemo.Years, nemo.Color);
            Console.WriteLine("Dzwięk: {0}",nemo.Sound());
            Console.WriteLine("Poruszanie: {0}",nemo.Move());
*/
            
            
            /*
            // Calculator
            
            while ( true )
            {
                var wyborLiczba = JakieDzialanie();

                if ( wyborLiczba == Dzialanie.Wyjscie )
                    break;

                if ( wyborLiczba == Dzialanie.WyczyscWynik )
                    continue;

                var pierwszaliczbaWartosc = 0d;

                if ( wynik is null )
                    pierwszaliczbaWartosc = PodajLiczbe( "pierwszą" );
                else
                    pierwszaliczbaWartosc = wynik ?? 0d;

                var drugaliczbaWartosc = PodajLiczbe( "drugą" );

                Console.WriteLine( new string( '-', 48 ) ); // wyswietla linie

                LiczWynik( wyborLiczba, pierwszaliczbaWartosc, drugaliczbaWartosc );
            } // [end] while
            */

        } // [end] Main


        public static Dzialanie JakieDzialanie()
        {

            int szukanyIndeks = -1;

            int wyborLiczba = -1;
            var result = Dzialanie.Inny;
            do
            {
                Console.WriteLine( new string( '-', 48 ) ); // wyswietla linie

                Console.WriteLine( @"Wybierz działanie:" );

                foreach ( var elementTablicy in tablicaDzialan )
                {
                    if ( elementTablicy == 0 )
                        Console.WriteLine();

                    Console.WriteLine( "{0}. {1}", ((int)elementTablicy).ToString(), OpisDzialania( elementTablicy ) );
                }

                var wybor = Console.ReadLine();

                try
                {
                    wyborLiczba = int.Parse( wybor! );
                }
                catch ( Exception ex )
                {
                    Console.WriteLine( "Błędny wybor !!! - to nie liczba" );
                    wyborLiczba = -1;
                    continue;
                }

                szukanyIndeks = Array.FindIndex( tablicaDzialan, ( i ) => (int)i == wyborLiczba );
                if ( szukanyIndeks < 0 )
                    Console.WriteLine( "Błędny wybór" );
                else
                    result = tablicaDzialan[ szukanyIndeks ];


            } while ( szukanyIndeks < 0 );


            if ( result == Dzialanie.WyczyscWynik )
            {
                wynik = null;
                Console.WriteLine( "Wynik wyczyszczony" );
            }

            return result;
        }

 
        public static void LiczWynik( Dzialanie wybor, double liczba1, double liczba2 )
        {
            if ( wybor == Dzialanie.Dodawanie ) // dodawanie
                wynik = Add( liczba1, liczba2 );

            if ( wybor == Dzialanie.Odejmowanie ) // odejmowanie
                wynik = Subtraction( liczba1, liczba2 );

            if ( wybor == Dzialanie.Mnozenie ) // mnożenie
                wynik = Multiplication( liczba1, liczba2 );

            if ( wybor == Dzialanie.Dzielenie ) // dzielenie
                wynik = Division( liczba1, liczba2 );

            if ( wybor == Dzialanie.ResztaZDzielenia ) // reszta z dzielenia
                wynik = Modulo( liczba1, liczba2 );

            if ( wybor == Dzialanie.Potegowanie ) // Potęgowanie
                wynik = Power( liczba1, liczba2 );

            if ( wybor == Dzialanie.Maksymalna ) // Maksymalna
                wynik = MaxLiczba( liczba1, liczba2 );

            if ( wybor == Dzialanie.Minimalna ) // Minimalna
                wynik = MinLiczba( liczba1, liczba2 );
        }

        private static double? MaxLiczba( double liczba1, double liczba2 )
        {
            double? _wynik = Math.Max( liczba1, liczba2 );
            PrintScore( liczba1, liczba2, _wynik, "max" );
            return _wynik;
        }
        
        private static double? MinLiczba( double liczba1, double liczba2 )
        {
            double? _wynik = Math.Min( liczba1, liczba2 );
            PrintScore( liczba1, liczba2, _wynik, "min" );
            return _wynik;
        }
        
        
        private static double? Power( double liczba1, double liczba2 )
        {
            double? _wynik = Math.Pow( liczba1, liczba2 );
            PrintScore( liczba1, liczba2, _wynik, "^" );
            return _wynik;
        }

        public static double? Add( double liczba1, double liczba2 )
        {
            double? wynik = liczba1 + liczba2;
            PrintScore( liczba1, liczba2, wynik, "+" );
            return wynik;
        }

        public static double? Subtraction( double liczba1, double liczba2 )
        {
            double? wynik = liczba1 - liczba2;
            PrintScore( liczba1, liczba2, wynik, "-" );
            return wynik;
        }

        public static double? Multiplication( double liczba1, double liczba2 )
        {
            double? wynik = liczba1 * liczba2;
            PrintScore( liczba1, liczba2, wynik, "*" );
            return wynik;
        }

        public static double? Division( double liczba1, double liczba2 )
        {
            double? wynik = liczba1 / liczba2;
            PrintScore( liczba1, liczba2, wynik, "/" );
            return wynik;
        }

        public static double? Modulo( double liczba1, double liczba2 )
        {
            double? wynik = liczba1 % liczba2;
            PrintScore( liczba1, liczba2, wynik, "%" );
            return wynik;
        }


        public static void PrintScore( double liczba1, double liczba2, double? _wynik, string znak )
        {
            Console.WriteLine( "{0} {3} {1} = {2}", liczba1, liczba2, _wynik?.ToString() ?? "(null)", znak );
        }


        public static double PodajLiczbe( string dodatkowyTekst )
        {
            double result = 0.0d;
            bool czyKoniecPelti = false;
            do
            {
                Console.WriteLine( "Podaj {0} liczbę:", dodatkowyTekst );

                var liczbaTekstowo = Console.ReadLine();

                try
                {
                    result = double.Parse( liczbaTekstowo ?? "" );
                    czyKoniecPelti = true;
                }
                catch ( FormatException ex )
                {
                    Console.WriteLine( "Błędny format liczby {0}", dodatkowyTekst );
                    continue;
                }
            } while ( czyKoniecPelti == false );

            return result;
        }
    }
}