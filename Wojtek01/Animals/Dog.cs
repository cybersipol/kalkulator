﻿using System;

namespace Wojtek01.Animals
{
    public abstract class Dog: Animal, ILandAnimal
    {
        public bool CzyMaWlosy = false;
        public string Race = string.Empty;
        
        public Dog(string name, string race, bool czyWlosy=false, string color = "white", int years = 0): base(name, color, years)
        {
            CzyMaWlosy = czyWlosy;
            Race = race;
            Console.WriteLine("Buduję Dog {0}", Name);
        }

        public string Sound()
        {
            return "hou hou";
        }

        public override string Move()
        {
            return "run";
        }

        public abstract override void OddajStolec();

        public override string ToString()
        {
            
            var result = base.ToString();

            result += Environment.NewLine + "Rasa: " + Race;
            result += Environment.NewLine + "Czy ma włosy?: " + (CzyMaWlosy ? "tak":"nie");

            return result;

        }
    }
}