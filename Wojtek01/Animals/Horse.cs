﻿using System;

namespace Wojtek01.Animals
{
    public class Horse : Animal, ILandAnimal
    {
        public Horse( string name, string color = "white", int years = 0 ) : base( name, color, years )
        {
            Console.WriteLine("Buduje konia {0}", Name);
        }

        public string Sound()
        {
            return "i ha ha";
        }

        public override string Move()
        {
            return "run";
        }

        public override void OddajStolec()
        {
            throw new NotImplementedException();
        }
    }
}