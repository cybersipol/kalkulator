﻿using System;

namespace Wojtek01.Animals
{
    public class Fish: Animal
    {
        public bool CzySlonoWodna;
        
        public Fish( string name, bool czySlonoWodna=false, string color = "white", int years = 0 ) : base( name, color, years )
        {
            CzySlonoWodna = czySlonoWodna;
            Console.WriteLine("Buduje rybe {0}", Name);
        }

        
        public override string Move()
        {
            return "swim";
        }

        public override void OddajStolec()
        {
            throw new NotImplementedException();
        }
    }
}