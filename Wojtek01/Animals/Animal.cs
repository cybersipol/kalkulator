﻿using System;

namespace Wojtek01.Animals
{
    public abstract class Animal: IMove, IName
    {
        public string Name
        {
            get
            {
                return _name ?? string.Empty;
            }
            private set { _name = value ?? "jakis zwierzak"; }
        }
        
        
        private string _name;
        protected string Color;
        public int Years { get; private set; }

        public Animal(string _name, string color = "white", int years = 0)
        {
            Name = _name;
            this.Color = color;
            this.Years = years;
            
            Console.WriteLine("Buduję Animal {0} koloru {1}", _name, color );
        }

        
        public Animal GetAnimal()
        {
            return this;
        }

        public virtual string Move()
        {
            return "some move";
        }

        public abstract void OddajStolec();
        

        public override string ToString()
        {
            var result = Environment.NewLine;

            result = result + String.Format( "--- {0} : {1} ---", _name, GetType().Name ) + Environment.NewLine;
            result += String.Format( "Wiek {0}, kolor: {1}", Years, Color ) + Environment.NewLine;
            
            if (this is ISound)
                result += "Dzwięk: " + (this as ISound).Sound() + Environment.NewLine;
            
            result += "Poruszanie: " + Move();

            return result;
        }

    }
}