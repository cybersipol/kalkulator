﻿using System;

namespace Wojtek01.Animals
{
    public class Lablador: Dog
    {
        public static readonly bool CzySiersc = true;
        
        public Lablador( string name, string color = "white", int years = 0 ) : base( name, "Labek", !CzySiersc, color, years )
        {
            Console.WriteLine("Buduję Labladora {0}", Name);
        }

        public override void OddajStolec()
        {
            
            throw new NotImplementedException();
        }

        public static int pobierzSredniWiek()
        {
            return 7;
        }
    }
}