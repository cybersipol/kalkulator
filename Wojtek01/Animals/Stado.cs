﻿using System.Text;

namespace Wojtek01.Animals
{
    public class Stado: object
    {
        private ILandAnimal[] _animals;

        public Stado(ILandAnimal[] animals)
        {
            _animals = animals;
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( "" );
            foreach ( var animal in _animals )
            {
                sb.AppendLine(animal.ToString());
            }

            return sb.ToString();

        }
    }
}